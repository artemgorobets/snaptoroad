package com.artemgorobets.snaptoroadexample;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.artemgorobets.snaptoroadexample.geo.GeoHelper;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MapsActivity extends FragmentActivity implements LocationListener {

    private GoogleMap mMap;
    LatLng gpsPosition;
    LatLng correctedPosition;
    JSONObject response;
    List<LatLng> roadPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            response = new JSONObject(new MockHelper().getJSONResponse(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        roadPoints = new MockHelper().getPoints(this);
        drawPolyLines(roadPoints);
        initLocationManager();
    }

    private void initLocationManager() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, this);
    }

    private void updateMarkerPosition(){

        correctedPosition = getFixedLocation();

        mMap.addMarker(
                new MarkerOptions()
                        .position(gpsPosition)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.orange_marker)));

        if (correctedPosition != null)

            mMap.addMarker(
                new MarkerOptions()
                        .position(correctedPosition)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.blue_marker)));
    }

    private void drawPolyLines(List<LatLng> pointList){
        PolylineOptions options = new PolylineOptions().width(8).color(Color.BLUE).geodesic(true);
        for (int i = 0; i < pointList.size(); i++) {
            LatLng point = pointList.get(i);
            options.add(point);
        }
        mMap.addPolyline(options);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    @Override
    public void onLocationChanged(Location location) {
        String str = "Latitude: "+location.getLatitude()+"Longitude: "+location.getLongitude();
        gpsPosition = new LatLng(location.getLatitude(), location.getLongitude());

        updateMarkerPosition();
        Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getBaseContext(), "Gps turned on ", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getBaseContext(), "Gps turned off ", Toast.LENGTH_LONG).show();
    }

    private LatLng getFixedLocation() {
        GeoHelper geoHelper = new GeoHelper();
        return geoHelper.getFixedPosition(roadPoints, gpsPosition);
    }


}
