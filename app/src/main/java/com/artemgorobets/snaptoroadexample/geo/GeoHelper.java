package com.artemgorobets.snaptoroadexample.geo;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Artem Gorobets on 24.05.2015.
 */
public class GeoHelper {

    private static Location nearestPointOnLine(double ax, double ay,
                                               double bx, double by,
                                               double px, double py,
                                               boolean clampToSegment) {

        Location location = new Location("");

        double apx = px - ax;
        double apy = py - ay;
        double abx = bx - ax;
        double aby = by - ay;

        double ab2 = abx * abx + aby * aby;
        double ap_ab = apx * abx + apy * aby;
        double t = ap_ab / ab2;

        if (clampToSegment) {
            if (t < 0) {
                t = 0;
            } else if (t > 1) {
                t = 1;
            }
        }

        location.setLongitude(ax + abx * t);
        location.setLatitude(ay + aby * t);

        return location;
    }

    public LatLng getFixedPosition(List<LatLng> roadPoints, LatLng pos) {
        int closestSegment = 0;
        double distance;
        double newDistance;

        double ax;
        double ay;
        double bx;
        double by;

        if (roadPoints != null && roadPoints.size() > 0) {

            distance = calculateDistance(
                    roadPoints.get(0).latitude, roadPoints.get(0).longitude, pos);
            int i = 0;
            while (i < roadPoints.size()) {
                double nodeLat = roadPoints.get(i).latitude;
                double nodeLng = roadPoints.get(i).longitude;
                newDistance = calculateDistance(nodeLat, nodeLng, pos);

                if (newDistance < distance) {
                    distance = newDistance;
                    closestSegment = i;
                }

                i++;
            }

            ax = roadPoints.get(closestSegment).longitude;
            ay = roadPoints.get(closestSegment).latitude;

            if (closestSegment % 2 == 0) {
                bx = roadPoints.get(closestSegment + 1).longitude;
                by = roadPoints.get(closestSegment + 1).latitude;
            } else {
                bx = roadPoints.get(closestSegment - 1).longitude;
                by = roadPoints.get(closestSegment - 1).latitude;
            }

            Location location = nearestPointOnLine(ax, ay, bx, by, pos.longitude, pos.latitude, true);
            return new LatLng(location.getLatitude(), location.getLongitude());

        } else {
            return new LatLng(pos.latitude, pos.longitude);
        }
    }

    private double calculateDistance(Double nodeLat, Double nodeLng, LatLng currentPosition) {
        return Math.sqrt(
                (nodeLng - currentPosition.longitude) * (nodeLng - currentPosition.longitude) +
                        (nodeLat - currentPosition.latitude) * (nodeLat - currentPosition.latitude));
    }

}
