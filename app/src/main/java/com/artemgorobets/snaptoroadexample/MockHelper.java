package com.artemgorobets.snaptoroadexample;

import android.content.Context;

import com.artemgorobets.snaptoroadexample.geo.PolyLineEncoder;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artem on 20.05.2015.
 */
public class MockHelper {

    public String getJSONResponse(Context context) {
        int ctr;
        InputStream inputStream = context.getResources().openRawResource(R.raw.mock);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toString();
    }

    public List<LatLng> getPoints(Context context){
        List<LatLng> pointList = new ArrayList<>();
        try {
            JSONObject jObject = new JSONObject(
                    getJSONResponse(context));

            JSONArray jArray = jObject.getJSONArray("routes");
            JSONObject encodedPolylineObject = jArray.getJSONObject(0).getJSONObject("overview_polyline");
            String encoded = encodedPolylineObject.getString("points");
            pointList = PolyLineEncoder.decodePolyline(encoded);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return pointList;
    }


}
